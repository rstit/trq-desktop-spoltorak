'use strict';
module.exports = (sequelize, DataTypes) => {
  var Material = sequelize.define('material', {
    name: DataTypes.STRING,
    permeability: DataTypes.FLOAT,
    resistance: DataTypes.FLOAT,
    frequency: DataTypes.FLOAT
  }, {});
  Material.associate = function(models) {
    // associations can be defined here
    models.material.hasMany(models.scan);
  };
  return Material;
};
