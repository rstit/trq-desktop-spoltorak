const output = [];
const input = [];

const registerOutputHandler = handler => output.push(handler);
const registerInputHandler = handler => input.push(handler);

const logOutput = (buffer) => output.forEach(callback => callback(buffer, new Date()));
const logInput = (buffer) => input.forEach(callback => callback(buffer, new Date()));

const decorateWriteWithLogger = (socket) => {
  const write = socket.write;
  socket.write = (buffer) => {
    logOutput(buffer);
    write.call(socket, buffer);
  }
}

module.exports = {
  decorateWriteWithLogger,
  logOutput,
  logInput,
  registerInputHandler,
  registerOutputHandler,
};
