const {
  DEVICE_PORT,
  FAKE_DEVICE_HOST,
 } = require('../src/backend/constants');

var blessed = require('blessed');

var screen = blessed.screen({
  smartCSR: true
});
screen.title = `Device running on ${FAKE_DEVICE_HOST}:${DEVICE_PORT}`;

const renderState = state => `\n
    Client connected: ${state.connected}
    Frequency: ${state.frequencyTuning} Hz
    Trigger pressed: ${state.triggerPressed}
`;

const getScanButtonText = state => state.triggerPressed ? ' Scanning... ' : 'Press to scan';

const stateBox = blessed.box({
  parent: screen,
  left: 0,
  top: 0,
  width: 'half',
  height: 6,
  bg: 'green',
  label: 'Current state:',
});

const form = blessed.form({
  parent: screen,
  keys: true,
  left: 0,
  top: 7,
  width: 'half',
  height: 8,
  bg: 'green',
  label: 'Controls:',
  content: '\n\n    Device is ready for scanning:'
});

const inputBox = blessed.box({
  parent: screen,
  width: 'half',
  height: 'half',
  right: 0,
  top: 0,
  border: 'line',
  label: 'Input:',
  scrollable: true,
  alwaysScroll: true,
  keys: true,
  vi: true,
  scrollbar: {
    ch: ' ',
    inverse: true
  },
  style: {
    scrollbar: {
      bg: 'blue'
    },
  },
  padding: {
    top: 0
  }
});

const outputBox = blessed.box({
  parent: screen,
  width: 'half',
  height: 'half',
  right: 0,
  top: '50%',
  border: 'line',
  label: 'Output:',
  scrollable: true,
  alwaysScroll: true,
  keys: true,
  vi: true,
  scrollbar: {
    ch: ' ',
    inverse: true
  },
  style: {
    scrollbar: {
      bg: 'blue'
    },
  },
  padding: {
    top: 0
  }
});

const helpBox = blessed.box({
  parent: screen,
  left: 0,
  top: 16,
  width: 'half',
  bg: 'black',
  label: '\nHelp:',
  content: `

    q - quit
    s - start or stop scanning
    d - disconnect from the client`
});

const scanButton = blessed.button({
  parent: form,
  mouse: true,
  keys: true,
  shrink: true,
  left: 5,
  top: 4,
  padding: {
    top: 1,
    left: 2,
    right: 2,
    bottom: 1,
  },
  name: 'trigger',
  style: {
    bg: 'blue',
    focus: {
      bg: 'red'
    },
    hover: {
      bg: 'red'
    }
  }
});

const disconnectButton = blessed.button({
  parent: form,
  mouse: true,
  keys: true,
  shrink: true,
  left: 24,
  top: 4,
  name: 'trigger',
  content: 'Disconnect',
  padding: {
    top: 1,
    left: 2,
    right: 2,
    bottom: 1
  },
  style: {
    bg: 'blue',
    focus: {
      bg: 'red'
    },
    hover: {
      bg: 'red'
    }
  }
});

const alertButton = blessed.button({
  parent: form,
  mouse: true,
  keys: true,
  shrink: true,
  left: 44,
  top: 4,
  content: 'Send example alert',
  padding: {
    top: 1,
    left: 2,
    right: 2,
    bottom: 1
  },
  style: {
    bg: 'blue',
    hover: {
      bg: 'red'
    }
  }
});

scanButton.on('press', () => {
  ui.toggleScanClicked();
  screen.render();
});

disconnectButton.on('press', () => {
  ui.disconnectClicked();
  screen.render();
});

alertButton.on('press', () => {
  ui.alertClicked();
  screen.render();
});


screen.key('q', function() {
  process.exit(0);
});

screen.key('s', function() {
  ui.toggleScanClicked();
  screen.render();
});

screen.key('d', function() {
  ui.disconnectClicked();
  screen.render();
});

const leadZero = (value) => {
  const string = value.toString();
  if (string.length == 1) {
    return `0${value}`;
  }
  return string;
}

const formatBuffer = (buffer, date) => {
  const data = Array.from(buffer.values()).map(value => value.toString(16)).map(leadZero).join(' ');
  const h = date.getHours();
  const m = date.getMinutes();
  const s = date.getSeconds();
  const time = `${leadZero(h)}:${leadZero(m)}:${leadZero(s)}`;
  return `[${time}]: ${data}`;
};

screen.render();

const ui = {
  toggleScanClicked: () => {},
  disconnectClicked: () => {},
  alertClicked: () => {},
  setState: (newState) => {
    stateBox.setContent(renderState(newState));
    scanButton.setContent(getScanButtonText(newState));
    screen.render();
  },
  addOutput: (buffer, date) => {
    const output = formatBuffer(buffer, date);
    outputBox.pushLine(output);
    outputBox.setScrollPerc(100);
    screen.render();
  },
  addInput: (buffer, date) => {
    const input = formatBuffer(buffer, date);
    inputBox.pushLine(input);
    inputBox.setScrollPerc(100);
    screen.render();
  },
}

module.exports = ui;
