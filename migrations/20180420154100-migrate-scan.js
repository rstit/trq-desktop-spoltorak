'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const tableDescription = await queryInterface.describeTable('scans');
    if (tableDescription.measurements) {
      return;
    }
    return queryInterface.addColumn('scans', 'measurements', {
      type: Sequelize.JSON
    });
  },
  down: async (queryInterface, Sequelize) => {
    const tableDescription = await queryInterface.describeTable('scans');
    if (!tableDescription.measurements) {
      return;
    }
    return queryInterface.removeColumn('scans', 'measurements', {
      type: Sequelize.JSON
    });
  }
};
