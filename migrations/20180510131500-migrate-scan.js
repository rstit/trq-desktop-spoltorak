'use strict';
const UNKNOWN = 'unknown';
const INVALID = 'invalid';
const VALID = 'valid';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const tableDescription = await queryInterface.describeTable('scans');
    if (tableDescription.outcome) {
      return;
    }
    return queryInterface.addColumn('scans', 'outcome', {
      type: Sequelize.ENUM(UNKNOWN, INVALID, VALID),
      allowNull: false,
      defaultValue: UNKNOWN
    });
  },
  down: async (queryInterface, Sequelize) => {
    const tableDescription = await queryInterface.describeTable('scans');
    if (!tableDescription.outcome) {
      return;
    }
    return queryInterface.removeColumn('scans', 'outcome');
  }
};
