'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const tableDescription = await queryInterface.describeTable('scans');
    if (tableDescription.pending) {
      return;
    }
    return queryInterface.addColumn('scans', 'pending', {
      type: Sequelize.BOOLEAN
    });
  },
  down: async (queryInterface, Sequelize) => {
    const tableDescription = await queryInterface.describeTable('scans');
    if (!tableDescription.pending) {
      return;
    }
    return queryInterface.removeColumn('scans', 'pending', {
      type: Sequelize.BOOLEAN
    });
  }
};
