import { Component, OnInit, NgZone } from '@angular/core';
import { ConnectionService } from '../connection/connection.service';
import wifi from '../../communication/wifi';

@Component({
  selector: 'trq-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.scss']
})
export class ConnectionComponent implements OnInit {
  isConnectedToWifi: boolean;

  constructor(private _connectionService: ConnectionService, private _ngZone: NgZone) { }

  ngOnInit() {
    wifi.isConnected().then(result => {
      this._ngZone.run(() => {
        this.isConnectedToWifi = result;
      });
    });
  }

  isConnectedToScanner(): boolean {
    return this._connectionService.scannerData.state === 'connected';
  }
}
