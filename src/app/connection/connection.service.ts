import { Injectable, NgZone } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { connectToBackend, scannerObservable, ScannerMessageType, ScannerStatusType,
         IScannerMessage, IScannerStatusData } from '../../communication/scanner.observable';

@Injectable()
export class ConnectionService {
  private _scannerSubscription: Subscription;

  scannerData: IScannerStatusData = {
    state: ScannerStatusType.disconnected,
    battery: 0,
    deviceName: null,
    deviceId: null,
    firmwareVersion: null,
    deviceStatus: null,
  };

  constructor(private _ngZone: NgZone) {
    connectToBackend();
    this._scannerSubscription = scannerObservable.subscribe((value: IScannerMessage) => {
      this._ngZone.run(() => {
        if (value.messageType === ScannerMessageType.status) {
          this.scannerData = Object.assign(this.scannerData, value.data);
        }
      });
    });
  }

  batteryLevelClassNumber() {
    return Math.ceil(this.scannerData.battery * 10);
  }
}
