import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannerConnectedComponent } from './scanner-connected.component';

describe('ScannerConnectedComponent', () => {
  let component: ScannerConnectedComponent;
  let fixture: ComponentFixture<ScannerConnectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScannerConnectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScannerConnectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
