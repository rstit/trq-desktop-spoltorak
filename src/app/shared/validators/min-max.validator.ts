import { ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { AbstractControl } from '@angular/forms';

function isEmptyInputValue(value: any): boolean {
  return value == null || value.length === 0;
}

export function minValidator(min: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (isEmptyInputValue(control.value)) {
      return null; // don't validate empty values to allow optional controls
    }
    const value = parseFloat(control.value);
    return isNaN(value) || value < min ? { 'min': { 'min': min, 'actual': control.value } } : null;
  };
}

export function maxValidator(max: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (isEmptyInputValue(control.value)) {
      return null; // don't validate empty values to allow optional controls
    }
    const value = parseFloat(control.value);
    return isNaN(value) || value > max ? { 'max': { 'max': max, 'actual': control.value } } : null;
  };
}
