import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'trq-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss']
})
export class InputSearchComponent implements OnInit {
  @Input() query;
  @Output() queryChange: EventEmitter<string> = new EventEmitter();
  @Output() querySubmit: EventEmitter<any> = new EventEmitter();

  constructor() {}

  get model() {
    return this.query;
  }
  set model(value) {
    this.queryChange.emit(value);
  }

  ngOnInit() {
  }

  cancel() {
    this.queryChange.emit(null);
  }
}
