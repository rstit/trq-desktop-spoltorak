import { Pipe, PipeTransform } from '@angular/core';
import * as humanFormat from 'human-format';
import { UNITS } from '../constants';

@Pipe({
  name: 'unit'
})
export class UnitPipe implements PipeTransform {
  transform(value: string, unitType: string): any {
    const number = parseFloat(value);
    const unit = UNITS[unitType];
    if (!unit || Number.isNaN(number)) {
      return value;
    }
    const raw = humanFormat.raw(number, {
      unit,
      decimals: 2
    });
    return `${raw.value} ${raw.prefix}${unit}`;
  }
}
