import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import titlebar from './titlebar';

import '../styles/app.scss';

const backButtonBlackList = [
  '/',
  '/connection',
];

@Component({
  selector: 'trq-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'eddysense';

  constructor(private _router: Router) {
    titlebar.setTitle(this.title);
    titlebar.registerBackEventHandler(() => {
      if (history.length > 1) {
        history.back();
      }
    });
    this._router.events.subscribe(event => {
      if (!(event instanceof NavigationEnd)) {
        return;
      }
      if (backButtonBlackList.includes(event.url)) {
        titlebar.hideBackButton();
        return;
      }
      titlebar.showBackButton();
    });
  }
}
