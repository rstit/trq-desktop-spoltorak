import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { IMaterial } from '../../../communication/Materials.adapter';
import materialsAdapter from '../../../communication/materials';

@Component({
  selector: 'trq-materials-list-view',
  templateUrl: './materials-list-view.component.html',
  styleUrls: ['./materials-list-view.component.scss']
})
export class MaterialsListViewComponent implements OnInit, OnChanges {
  @Input() query: string;
  @Input() selectedMaterialId: number;
  @Output() selectedMaterialIdChange: EventEmitter<number> = new EventEmitter();

  offset = 0;
  limit = 36; // should be divisible by 3
  hasNextPage = true;

  materials: IMaterial[];

  constructor() { }

  ngOnInit() {
    this.getMaterials();
  }

  ngOnChanges(changes) {
    if (changes.query) {
      this.getMaterials();
    }
  }

  getMaterials(offset = this.offset, limit = this.limit, reload = true) {
    if (reload) {
      this.offset = 0;
    }
    const request = this.query ? materialsAdapter.search({
      query: this.query,
      offset,
      limit,
    }) : materialsAdapter.findAll({
      offset,
      limit,
    });

    return request
      .then(result => {
        this.hasNextPage = !!result.length;
        if (reload) {
          this.materials = <IMaterial[]>result;
        }
        else {
          this.materials = this.materials.concat(<IMaterial[]>result);
        }
      })
      .catch(error => console.warn('Error when getting materials: ', error));
  }

  infiniteScrollCallback(event) {
    return this.getMaterials(event.limit, event.offset, false)
      .then(data => {
        if (typeof (event.callback) === 'function') { event.callback(); }
        else { throw new Error('Infinite scroll callback is not a function or is not defined'); }
      });
  }

  isMaterialSelected(material: IMaterial) {
    return this.selectedMaterialId === material.id;
  }
}
