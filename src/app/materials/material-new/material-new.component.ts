import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IMaterial } from '../../../communication/Materials.adapter';
import materialsAdapter from '../../../communication/materials';

@Component({
  selector: 'trq-material-new',
  templateUrl: './material-new.component.html',
  styleUrls: ['./material-new.component.scss']
})
export class MaterialNewComponent implements OnInit {
  material: IMaterial = {
    name: null,
    permeability: null,
    resistance: null,
    frequency: null
  };

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  saveMaterial(material: IMaterial) {
    materialsAdapter.create(material)
      .then(result => this._router.navigate(['/materials']))
      .catch(error => console.warn('Error while getting material with id ${materialId} : ', error));
  }
}
