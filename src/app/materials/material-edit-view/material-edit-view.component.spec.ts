import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialEditViewComponent } from './material-edit-view.component';

describe('MaterialEditViewComponent', () => {
  let component: MaterialEditViewComponent;
  let fixture: ComponentFixture<MaterialEditViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialEditViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialEditViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
