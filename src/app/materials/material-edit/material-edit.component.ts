import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { IMaterial } from '../../../communication/Materials.adapter';
import materialsAdapter from '../../../communication/materials';

@Component({
  selector: 'trq-material-edit',
  templateUrl: './material-edit.component.html',
  styleUrls: ['./material-edit.component.scss']
})
export class MaterialEditComponent implements OnInit {
  material: IMaterial;

  constructor(private route: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.getMaterial(params['id']);
    });
  }

  getMaterial(materialId) {
    materialsAdapter.findById(materialId)
      .then(result => this.material = result)
      .catch(error => console.warn('Error while getting material with id ${materialId} : ', error));
  }

  saveMaterial(material: IMaterial) {
    materialsAdapter.updateById(material.id, material)
      .then(result => this._router.navigate(['/materials']))
      .catch(error => console.warn('Error while updating material with id ${materialId} : ', error));
  }

  deleteMaterial() {
    materialsAdapter.destroyById(this.material.id)
      .then(result => this._router.navigate(['/materials']))
      .catch(error => console.warn('Error while deleting material with id ${materialId} : ', error));
  }

}
