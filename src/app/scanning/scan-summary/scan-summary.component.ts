import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IScan, Outcome } from '../../../communication/ScanningProcess.adapter';
import EditableScan from '../../scanning/scans/scan-edit-form/editable-scan.abstract';

@Component({
  selector: 'trq-scan-summary',
  templateUrl: './scan-summary.component.html',
  styleUrls: ['./scan-summary.component.scss']
})
export class ScanSummaryComponent extends EditableScan implements OnInit {
  Outcome = Outcome;
  @Input() scan: IScan = {
    id: null,
    partNumber: null,
    manufacturer: null,
    description: null,
    detectionDepth: null,
    frequency: null,
    additionalInfo: {},
    material: null
  };
  @Output() editScan: EventEmitter<IScan> = new EventEmitter();
  @Output() deleteScan: EventEmitter<IScan> = new EventEmitter();
  @Output() retryScan: EventEmitter<IScan> = new EventEmitter();
  @Output() updateScan: EventEmitter<IScan> = new EventEmitter();

  ngOnInit() {
  }

  onEditClick() {
    this.editScan.emit(this.scan);
  }

  onDeleteClick() {
    this.deleteScan.emit(this.scan);
  }

  onRetryClick() {
    this.retryScan.emit(this.scan);
  }

}
