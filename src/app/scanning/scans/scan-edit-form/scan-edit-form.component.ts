import { Component, Input, Output, OnChanges, OnInit, EventEmitter } from '@angular/core';
import { IScan } from '../../../../communication/ScanningProcess.adapter';
import EditableScan from './editable-scan.abstract';

@Component({
  selector: 'trq-scan-edit-form',
  templateUrl: './scan-edit-form.component.html',
  styleUrls: ['./scan-edit-form.component.scss']
})
export class ScanEditFormComponent extends EditableScan implements OnInit, OnChanges {
  @Input() scan: IScan = {
    id: null,
    partNumber: null,
    manufacturer: null,
    description: null,
    detectionDepth: null,
    frequency: null,
    additionalInfo: {},
    material: null
  };
  @Input() isFullscreen: boolean;
  @Output() closeEdit: EventEmitter<void> = new EventEmitter();
  @Output() updateScan: EventEmitter<IScan> = new EventEmitter();

  ngOnInit() {
  }

  ngOnChanges(changes) {
    if (changes.scan) {
      this.manufacturerVisible = !!this.scan.manufacturer;
      if (this.scan.additionalInfo) {
        this.additionalInfoArrays = Object.entries(this.scan.additionalInfo);
      }
    }
  }

  onCloseEdit() {
    this.closeEdit.emit();
  }
}

