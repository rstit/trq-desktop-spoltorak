import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanEditFormComponent } from './scan-edit-form.component';

describe('ScanEditFormComponent', () => {
  let component: ScanEditFormComponent;
  let fixture: ComponentFixture<ScanEditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanEditFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
