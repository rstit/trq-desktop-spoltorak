import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScansShowComponent } from './scans-show.component';

describe('ScansShowComponent', () => {
  let component: ScansShowComponent;
  let fixture: ComponentFixture<ScansShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScansShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScansShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
