import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScansListViewComponent } from './scans-list-view.component';

describe('ScansListViewComponent', () => {
  let component: ScansListViewComponent;
  let fixture: ComponentFixture<ScansListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScansListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScansListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
