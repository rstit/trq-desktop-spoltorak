import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IScan, Outcome } from '../../../../communication/ScanningProcess.adapter';

@Component({
  selector: 'trq-scans-list-view',
  templateUrl: './scans-list-view.component.html',
  styleUrls: ['../scans-list.scss']
})
export class ScansListViewComponent {
  Outcome = Outcome;
  @Input() scansList: IScan[] = [];

  @Input() offset: number;
  @Input() limit: number;
  @Input() hasNextPage: boolean;
  @Input() recentScansIds: number[] = [];

  @Output() scanSelected: EventEmitter<IScan> = new EventEmitter();
  @Output() scansListInfiniteScrollCallback: EventEmitter<any> = new EventEmitter();

  constructor() { }

  hasRecentScans() {
    if (!Array.isArray(this.scansList) || !Array.isArray(this.recentScansIds)) {
      return;
    }
    return this.scansList.length > 0 && this.recentScansIds.length > 0;
  }

  infiniteScrollCallback(event) {
    this.scansListInfiniteScrollCallback.emit(event);
  }
}
