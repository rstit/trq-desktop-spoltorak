import { Component, OnInit } from '@angular/core';
import { IScan, Outcome } from '../../../../communication/ScanningProcess.adapter';
import { ActivatedRoute, Router } from '@angular/router';
import scansAdapter from '../../../../communication/scanningProcess';

@Component({
  selector: 'trq-scan-preview',
  templateUrl: './scan-preview.component.html',
  styleUrls: ['../scans-list.scss', './scan-preview.component.scss']
})
export class ScanPreviewComponent implements OnInit {
  Outcome = Outcome;
  scanId: number;
  scan: IScan = {
    id: null,
    partNumber: null,
    manufacturer: null,
    description: null,
    detectionDepth: null,
    frequency: null,
    additionalInfo: {},
    material: null,
    measurements: []
  };
  relatedScans: IScan[] = [];

  constructor(private route: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.scanId = params['id'];
      this.getScan();
    });
  }

  getScan() {
    scansAdapter.scanFindById(this.scanId)
      .then(result =>  {
        this.scan = result;
      })
      .catch(error => console.warn('Error while getting scan with id ${scanId} : ', error));

    scansAdapter.scanFindRelated(this.scanId)
      .then(result => this.relatedScans = result)
      .catch(error => console.warn('Error while getting scans related to scan with id ${scanId} : ', error));
  }

  scanSelected(scan) {
    this._router.navigate(['/scan-edit', scan.id]);
  }

  getScanAdditionalInfoKeys() {
    return Object.keys(this.scan.additionalInfo);
  }

  deleteScan() {
    scansAdapter.scanDestroyById(this.scanId)
      .then(result => this._router.navigate(['/scan-history']))
      .catch(error => console.warn('Error while deleting scan with id ${materialId} : ', error));
  }
}
