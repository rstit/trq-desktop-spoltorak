import { TestBed, inject } from '@angular/core/testing';

import { ScanningProcessService } from './scanning-process.service';

describe('ScanningProcessService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScanningProcessService]
    });
  });

  it('should be created', inject([ScanningProcessService], (service: ScanningProcessService) => {
    expect(service).toBeTruthy();
  }));
});
