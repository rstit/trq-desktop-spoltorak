const ipcRenderer = (<any>window).require('electron').ipcRenderer;

export function send<T>(commandName: string, payload?: any): Promise<T> {
  return new Promise((resolve, reject) => {
    ipcRenderer.send(commandName, payload);
    ipcRenderer.once(commandName, (sender, response) => {
      resolve(response);
    });
  });
}
