import { send } from '../lib/ipcAdapter';

export interface ISaveOptions {
  query: string;
  offset?: number;
  limit?: number;
  order?: string;
}
/**
 * Sends IPC message to the main process which open a save dialog window.

 * @param offset
 * @param limit
 * @param order
 */
export const saveScansToCSVFile = async ({
  query,
  offset = 0,
  limit,
  order = 'ASC'
}: ISaveOptions): Promise<boolean> => {
  return send<boolean>('saveScansToCSVFile', {
    query,
    offset,
    limit,
    order,
  });
};
