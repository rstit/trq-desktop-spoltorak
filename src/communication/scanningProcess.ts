/**
 * Instance of [[ScanningProcess.adapter]].
 */
import ScanningProcess from './ScanningProcess.adapter';
export default new ScanningProcess();
