import { send } from '../lib/ipcAdapter';

import { IMaterial } from '../communication/Materials.adapter';
import { UUID } from '../app/shared/constants';

export enum Outcome {
  Unknown = 'unknown',
  Invalid = 'invalid',
  Valid = 'valid'
}

interface IScanBaseProperties {
  partNumber: string;
  description: string;
  detectionDepth?: number;
  frequency: number;
}

export interface IMeasurement {
  delta: number;
  value: number;
}

export interface IScan extends IScanBaseProperties {
  id?: number;
  measurements?: IMeasurement[];
  manufacturer?: string;
  additionalInfo?: {[key: string]: string};
  material?: IMaterial;
  createdAt?: string;
  updatedAt?: string;
  sequenceId?: string;
  outcome?: Outcome;
}

export interface IScanSetup extends IScanBaseProperties {
  materialId: number;
  createdAt?: string;
  updatedAt?: string;
}

export interface IDDSConfig {
  frequencyTuning: number;
  sweepParameter1?: number;
  sweepParameter2?: number;
  risingDelta?: number;
  fallingDelta?: number;
  risingSweepRampRate?: number;
  fallingSweepRampRate?: number;
  phaseOffset?: number;
}

export interface IQueryOptions {
  offset?: number;
  limit?: number;
  order?: string;
}

export interface IQueryRelatedOptions {
  sequenceId: string;
  pending?: boolean;
}

export interface ISearchOptions {
  query: string;
  offset?: number;
  limit?: number;
}

export interface ICreateOptions {
  scanDefinition: IScan;
  sequenceId: UUID;
  materialId: number;
}

/**
 * Adapter for scanning process. We can:
 *
 * - creates a new scan sequence
 * - perform a new scan for newly sequence and material
 * - find all related scans for given scan.
 *
 * ```typescript
 * import ScanningProcess from './ScanningProcess.adapter';
 * const scanning = new ScanningProcess();
 *
 * const materialId = 1; // ID of a material, that is stored in the DB
 * const scanDefinition = {
 *   partNumber: "TRQ-1234",
 *   manufacturer: "Triteq",
 *   description: "Scan description",
 *   detectionDepth: 2.35,
 *   frequency: 1.2,
 *   additionalInfo: { foo: "bar" }
 * };

 * // Is an async function:
 * const sequenceId = await scanning.createSequence();
 * const scan = await scanning.scanCreateInSequenceForMaterial(scanDefinition, sequenceId, materialId);
 *
 * // We can now find all related scans, that ware created in the same sequence.
 * // scanFindRelated will return a promise which will contain an arrays of related
 * // scans, without the scan which ID was provided.
 * const relatedScans = await scanning.scanFindRelated(scan.id);
 * ```
 */
export default class ScanningProcess {
  static createSetupFromScan(scan: IScan): IScanSetup {
    return {
      partNumber: scan.partNumber,
      description: scan.description,
      frequency: scan.material.frequency,
      materialId: scan.material.id,
      createdAt: scan.createdAt,
      updatedAt: scan.updatedAt
    };
  }

  /**
   * @param scanDefinition
   * @param sequenceId id of a sequence in which the scan should be stored.
   * @param materialId id of a material for which the scan was performed.
   */
  async scanCreateInSequenceForMaterial({
    scanDefinition,
    sequenceId,
    materialId
  }: ICreateOptions): Promise<IScan> {
    return send<IScan>('scanCreateInSequenceForMaterial', { scanDefinition, sequenceId, materialId });
  }

  /**
   * scanFindRelated will return a promise which will contain an arrays of related
   * scans, without the scan which ID was provided.
   * @param scanId
   * @param offset
   * @param limit
   * @param order
   */
  async scanFindRelated(scanId: number, {
    offset = 0,
    limit,
    order = 'DESC'
  }: IQueryOptions = {}): Promise<IScan[]> {
    return send<IScan[]>('scanFindRelated', {
      scanId,
      offset,
      limit,
      order
    });
  }

  /**
   * scanFindAllInSequence will return a promise which will contain an arrays of all
   * scans in a sequnce
   * @param sequenceId
   * @param offset
   * @param limit
   * @param order
   */
  async scanFindAllInSequence({
    sequenceId,
    pending = false
  }: IQueryRelatedOptions): Promise<IScan[]> {
    return send<IScan[]>('scanFindAllInSequence', {
      sequenceId,
      pending
    });
  }

  /**
   * Returns a promise, which values in the ID of created sequnce.
   * This ID can be using during new scan creation.
   */
  async createSequence(): Promise<UUID> {
    return send<UUID>('createSequence');
  }

  /**
   * Updates a single scan by its id. Returns updates scan after.
   * @param scanId
   * @param scanDefinition
   */
  async scanUpdateById(scanId: number, scanDefinition: IScan): Promise<IScan> {
    return send<IScan>('scanUpdateById', {
      id: scanId,
      scan: scanDefinition
    });
  }

  /**
     * Returns a single scan by its id.
     * @param scanId
     */
  async scanFindById(scanId: number): Promise<IScan> {
    return send<IScan>('scanFindById', scanId);
  }

  /**
   * Removes a single scan by its id.
   * @param scanId
   */
  async scanDestroyById(scanId: number): Promise<void> {
    return send<void>('scanDestroyById', scanId);
  }

  async scanSearch({
    query,
    offset,
    limit
  }: ISearchOptions): Promise<IScan[]> {
    return send<IScan[]>('scanSearch', {
      query,
      offset,
      limit,
    });
  }

  async scanFindAll({
    offset,
    limit
  }: IQueryOptions): Promise<IScan[]> {
    return send<IScan[]>('scanFindAll', {
      offset,
      limit,
    });
  }

  async sendDDSConfig(config: IDDSConfig): Promise<void> {
    return send<void>('sendDDSConfig', config);
  }

  async markSequenceAsNotPending(sequenceId: UUID): Promise<void> {
    return send<void>('markSequenceAsNotPending', sequenceId);
  }

  async setScanningFrequency(frequency: number): Promise<void> {
    return send<void>('setFrequency', frequency);
  }
}
