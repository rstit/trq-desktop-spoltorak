import { send } from '../lib/ipcAdapter';

export interface IMaterial {
  id?: number;
  name: string;
  permeability: number;
  resistance: number;
  frequency: number;
  createdAt?: string;
  updatedAt?: string;
}

export interface IQueryOptions {
  offset: number;
  limit?: number;
  order?: string;
}

export interface ISearchOptions extends IQueryOptions {
  query: string;
}

/**
 * Materials adapter for communication with Node.js backend through IPC.
 * ```typescript
 * import Materials from './Materials.adapter';
 * const materials = new Materials();
 * materials.findAll({ offset: 1, limit: 10 }).then((results) => {
 *   // Prints array of 10 materials fetched from the database.
 *   console.log(results);
 * });
 * ```
 */
export default class Materials {
  /**
   * Fetches an array of materials from the database.
   *
   * @param offset
   * @param limit maximum number of materials to be returned
   * @param order can be ASC or DESC
   */
  async findAll({
    offset = 0,
    limit,
    order = 'ASC'
  }: IQueryOptions): Promise<IMaterial[]> {
    return send<IMaterial[]>('materialFindAll', {
      offset,
      limit,
      order,
    });
  }

  /**
   * Fetches array of materials matching the specified query (material name).
   *
   * @param query full or partial name of the material
   * @param offset
   * @param limit maximum number of materials to be returned
   * @param order can be ASC or DESC
   */
  async search({
    query,
    offset = 0,
    limit,
    order = 'ASC',
  }: ISearchOptions): Promise<IMaterial[]> {
    return send<IMaterial[]>('materialSearch', {
      query,
      offset,
      limit,
      order
    });
  }

  /**
   * Creates a new material. Will return promise which value will be the newly created material.
   * ```typescript
   * const material = await materials.create({
   *   name: 'Steel',
   *   permeability: 0.1,
   *   resistance: 0.223,
   *   frequency: 2.1,
   * });
   * console.log(material); // created material
   * ```
   * @param material Object with `material` interface.
   */
  async create(material: IMaterial): Promise<IMaterial> {
    return send<IMaterial>('materialCreate', material);
  }

  /**
   * Returns a single material by it's id.
   * @param id
   */
  async findById(id: number): Promise<IMaterial> {
    return send<IMaterial>('materialFindById', id);
  }

  /**
   * Updates a single material by it's id. Returns updated material after.
   * @param id
   * @param material
   */
  async updateById(id: number, material: IMaterial): Promise<IMaterial> {
    return send<IMaterial>('materialUpdateById', {
      id,
      material
    });
  }

  /**
   * Destroys a single material by it's id.
   * @param id
   */
  async destroyById(id: number): Promise<void> {
    return send<void>('materialDestroyById', id);
  }
}
