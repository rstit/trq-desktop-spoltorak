/**
 * Instance of [[WiFi.adapter]].
 */
import WiFi from './WiFi.adapter';
export default new WiFi();
