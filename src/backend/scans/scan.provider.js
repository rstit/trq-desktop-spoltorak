const Sequelize = require('sequelize');
const SORT_DESC = 'DESC';
const Op = Sequelize.Op;

class ScanProvider {
  constructor({ model, materialModel }) {
    this.model = model;
    this.materialModel = materialModel;
  }

  async search({
    query,
    offset,
    limit,
    order,
    pending = false
  } = {}) {
    const where = query ? {
      pending: false,
      [Op.or]: {
        partNumber: {
          [Op.like]: `%${query}%`,
        },
        manufacturer: {
          [Op.like]: `%${query}%`,
        },
        description: {
          [Op.like]: `%${query}%`,
        },
        detectionDepth: {
          [Op.like]: `%${query}%`,
        },
        frequency: {
          [Op.like]: `%${query}%`,
        },
        '$material.name$': {
          [Op.like]: `%${query}%`,
        }
      }
    } : undefined;
    return await this.findAll({
      offset,
      limit,
      order,
      pending,
      where
    });
  }

  async findAll({
    offset = 0,
    limit = 100,
    order = SORT_DESC,
    where = {},
    include = [ this.materialModel ],
    pending = false
  } = {}) {
    return await this.model.findAll({
      order: [
        ['createdAt', order]
      ],
      offset,
      limit,
      where: Object.assign({
        pending,
      }, where),
      include
    });
  }

  async findById(id, {
    include = [ this.materialModel ]
  } = {}) {
    return await this.model.findById(id, {
      include
    });
  }

  async create({
    partNumber,
    manufacturer,
    description,
    detectionDepth,
    frequency,
    additionalInfo,
    measurements,
  }) {
    return await this.model.create({
      partNumber,
      manufacturer,
      description,
      detectionDepth,
      frequency,
      additionalInfo,
      measurements,
    });
  }

  async updateById(id, fields) {
    const scan = await this.model.findById(id);
    if (!scan) {
      throw new Error(`Scan with id ${id} not found`);
    }
    Object.assign(scan, fields);
    return await scan.save({
      fields: Object.keys(fields)
    });
  }

  async destroyById(id) {
    const scan = await this.model.findById(id);
    if (!scan) {
      throw new Error(`Scan with id ${id} not found`);
    }
    return await scan.destroy();
  }
}

module.exports = ScanProvider;
