const Sequelize = require('sequelize');
const SORT_DESC = 'DESC';
const Op = Sequelize.Op;

const modelToJSON = result => result.get({ plain: true });

class SequenceProvider {
  constructor({ model }) {
    this.model = model;
  }

  async findAll({
    offset = 0, 
    limit = 100, 
    order = SORT_DESC,
    where
  }) {
    return await this.model.findAll({
      order: [
        ['createdAt', order]
      ],
      offset,
      limit,
      where,
    });
  }

  async findById(id) {
    return await this.model.findById(id);
  }

  async create() {
    return await this.model.create();
  }

  async updateById(id, fields) {
    const sequence = await this.model.findById(id);
    if (!sequence) {
      throw new Error(`Sequence with id ${id} not found`);
    }
    Object.assign(sequence, fields);
    return await sequence.save({
      fields: Object.keys(fields)
    });
  }
}

module.exports = SequenceProvider;
