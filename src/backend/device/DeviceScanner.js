const net = require('net');
const dns = require('dns');
const os = require('os');
const Promise = require('bluebird');
const networkList = require('network-list');
const DeviceClient = require('./../communication/DeviceClient')
const { DEVICE_PORT } = require('../constants');

const defautlOptions = {};
const COMPATIBLE_DEVICE_SEARCH_TIMEOUT_MS = 10000;

module.exports = class DeviceScanner {
  /**
   * Returns a promise with array of found devices.
   * Those are all devices found on the network we are currently connected.
   * They can be scanners, but that is not guaranteed.
  *
   * Single device object:
   * {
   *   ip: '192.168.1.1', // IP address
   *   alive: false, // true/false - device is alive
   *   hostname: null, // string - dns reverse hostname
   *   mac: null, // string - MAC address
   *   vendor: null, // string - vendor name
   *   hostnameError: null, // Error message if got error on getting hostname
   *   macError: null, // Error message if got an error on getting Mac Address
   *   vendorError: null, // Error message if got an error on getting vendor name
   * }
   */
  async scan(options = {}) {
    return new Promise((resolve, reject) => {
      networkList.scan(Object.assign({}, defautlOptions, options), (err, network) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(network);
      });
    });
  }

  /**
   * Returns a promise with found device that is a TRQ Scanner.
   */
  async findScanningDevice() {
    const rawDevices = await this.scan();
    const aliveRawDevices = rawDevices.filter(device => device.alive);
    const deviceClients = aliveRawDevices.map(this.createDeviceClientFromRawNetworkDevice);
    const connectionQueue = deviceClients.map(deviceClient => deviceClient.connect());
    const firstConnected = await this.findFirstCompatibleDevice(connectionQueue);
    const otherDevices = deviceClients.filter(deviceClient => deviceClient !== firstConnected);

    this.destroyDeviceClientsPendingConnections(otherDevices);

    return firstConnected;
  }

  createDeviceClientFromRawNetworkDevice(rawDevice) {
    const host = rawDevice.ip;
    const port = DEVICE_PORT;
    return new DeviceClient(port, host);
  }

  async destroyDeviceClientsPendingConnections(devices) {
    devices.forEach(deviceClient => {
      deviceClient.destroy();
    });
  }

  async findFirstCompatibleDevice(deviceClients) {
    return Promise.race(deviceClients)
      .timeout(COMPATIBLE_DEVICE_SEARCH_TIMEOUT_MS)
      .catch(Promise.TimeoutError, function() {
        return false;
      });
  }
}
