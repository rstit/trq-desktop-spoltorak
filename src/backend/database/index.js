const Sequelize = require('sequelize');
const sqlite = require('sqlite3');
const path = require('path');
const { app } = require('electron');
const Database = require('./Database');

const SQLITE3_DB_FILE_NAME = 'db.sqlite3';

const USER_DATA_PATH = app.getPath('userData');
const SQLITE3_DB_PATH = path.join(USER_DATA_PATH, SQLITE3_DB_FILE_NAME);
const database = new Database(SQLITE3_DB_PATH);
module.exports = database;
