const Sequelize = require('sequelize');
const Umzug = require('umzug');
const sqlite = require('sqlite3');
const fs = require('fs');
const path = require('path');
const initModels = require('../../../models');

class Database {
  static create(location) {
    // Creates the database file if it does not exists
    if (!fs.existsSync(location)) {
      new sqlite.Database(location);
    }
  }

  constructor(location) {
    Database.create(location);
    this.db = initModels(location);
  }

  migrate() {
    const umzug = new Umzug({
      storage: "sequelize",

      storageOptions: {
        sequelize: this.db.sequelize
      },

      migrations: {
        params: [
          this.db.sequelize.getQueryInterface(),
          this.db.Sequelize
        ],
        path: path.join(__dirname, "../../../migrations")
      }
    });

    return umzug.up();
  }

  authenticate() {
    return this.db.sequelize.authenticate();
  }

  sync(options) {
    return this.db.sequelize.sync(options);
  }

  getModel(modelName) {
    return this.db[modelName];
  }
}

module.exports = Database;
