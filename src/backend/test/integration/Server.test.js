const Server = require('../../server/Server');

const createIPCMock = () => ({
  on: jest.fn(),
});

describe('Server', () => {
  it('Initializes with all required ipc listeners', () => {
    const ipc = createIPCMock();
    const server = new Server({
      ipc,
    });
    expect(ipc.on.mock.calls[0][0]).toEqual('materialFindAll');
    expect(ipc.on.mock.calls[1][0]).toEqual('materialSearch');
    expect(ipc.on.mock.calls[2][0]).toEqual('materialCreate');
    expect(ipc.on.mock.calls[3][0]).toEqual('materialFindById');
  });
});
