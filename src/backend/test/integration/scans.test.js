const tmp = require('tmp');
const Database = require('../../database/Database');
const ScanProvicer = require('../../scans/scan.provider');

let scans;
let database;

const database_location = tmp.tmpNameSync();

const setup = async () => {
  database = new Database(database_location);
  scans = new ScanProvicer({
    model: database.getModel('scan'),
  });
  await database.authenticate();
  return await database.sync();
}

const tearDown = async () => {
  return await database.getModel('scan').drop();
}

describe('scans', () => {
  beforeEach(setup);
  afterEach(tearDown);

  const partNumber = 'part-1234';
  const manufacturer = 'Triteq';
  const description = 'desc';
  const detectionDepth = 1;
  const frequency = 1;
  const additionalInfo = {
    foo: 'bar',
  };
  it('creates scan', async () => {
    const result = await scans.create({
      partNumber,
      manufacturer,
      description,
      detectionDepth,
      frequency,
    });
    expect(result.partNumber).toEqual(partNumber);
    expect(result.manufacturer).toEqual(manufacturer);
    expect(result.description).toEqual(description);
    expect(result.detectionDepth).toEqual(detectionDepth);
    expect(result.frequency).toEqual(frequency);
  });

  it('creates scan with additional info as JSON', async () => {
    const result = await scans.create({
      partNumber,
      manufacturer,
      description,
      detectionDepth,
      frequency,
      additionalInfo,
    });
    const expectedAdditionalInfo = JSON.stringify(additionalInfo);
    expect(result.additionalInfo).toEqual(expectedAdditionalInfo);
  });
});
