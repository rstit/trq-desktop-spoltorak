const json2csv = require('json2csv');
const {dialog} = require('electron')

const Stream = require('stream');
const fs = require('fs');
const path = require('path');
const toJSON = scan => scan.get({ plain: true });

const saveDialogOptions = {
  title: 'Export scans',
  defaultPath: '*/scans.csv'
};

module.exports = class Exporter {
  constructor({ materialProvider, scanProvider}) {
    this.materialProvider = materialProvider;
    this.scanProvider = scanProvider;
  }

  async exportAllScans(...args) {
    const result = await this.scanProvider.search(...args);
    return result.map(toJSON)
      .map(scan => Object.assign(scan, { material: scan.material.name }));
  }

  async saveToCSV(data, outputPath) {
    if (!Array.isArray(data)) {
      throw new Error('[exporter] Provided data is not an array');
    }
    return new Promise((resolve, reject) => {
      const opts = {};
      const output = fs.createWriteStream(outputPath, { encoding: 'utf8' });
      const transformOpts = { highWaterMark: 16384, encoding: 'utf-8' };
      const readable = new Stream.Readable();
      const transform = new json2csv.Transform(opts, transformOpts);
      const stream = readable.pipe(transform).pipe(output);

      stream.on('finish', () => {
        resolve();
      });
      stream.on('error', (error) => {
        reject(error);
      });
      data.forEach(element => {
        readable.push(JSON.stringify(element));
      });
      readable.push(null);
    });
  }

  async showSaveDialog() {
    return new Promise((resolve, reject) => {
      dialog.showSaveDialog(saveDialogOptions, (filename) => {
        resolve(filename);
      });
    });
  }
}
