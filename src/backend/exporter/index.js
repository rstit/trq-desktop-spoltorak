const materialProvider = require('../materials');
const scanProvider = require('../scans');
const Exporter = require('./Exporter');

const exporter = new Exporter({ scanProvider, materialProvider });
// const csvExporter = new CSVExporter({ exporter });

module.exports = exporter;
