const protocol = require('./protocol');

const {
  BEEP_HEADER,
  BEEP_RESPONSE_HEADER,
  READ_DATA_HEADER,
  READ_DATA_RESPONSE_HEADER,
  WRITE_DATA_HEADER,
  FUNCTION_BEEP,
  WRITE_DATA_RESPONSE_HEADER,
  MEASURE_DATA_HEADER,
  HANDSHAKE,
  STATUS_STRUCT,
  BATTERY_STATUS_STRUCT,
  DDS_CONFIG_STRUCT,
  ATTENUATORS_CONFIG_STRUCT,
  BATTERY_CONFIG_STRUCT,
  WIFI_CONFIG_STRUCT,
  HANDSHAKE_SIZE,
  STATUS_STRUCT_SIZE,
  BATTERY_STATUS_STRUCT_SIZE,
  DDS_CONFIG_STRUCT_SIZE,
  ATTENUATORS_CONFIG_STRUCT_SIZE,
  BATTERY_CONFIG_STRUCT_SIZE,
  WIFI_CONFIG_STRUCT_SIZE,
} = require('../../constants');

const getStructSize = (struct_id) => {
  switch(struct_id) {
    case HANDSHAKE:
      return HANDSHAKE_SIZE;
    case STATUS_STRUCT:
      return STATUS_STRUCT_SIZE;
    case BATTERY_STATUS_STRUCT:
      return BATTERY_STATUS_STRUCT_SIZE;
    case DDS_CONFIG_STRUCT:
      return DDS_CONFIG_STRUCT_SIZE;
    case ATTENUATORS_CONFIG_STRUCT:
      return ATTENUATORS_CONFIG_STRUCT_SIZE;
    case BATTERY_CONFIG_STRUCT:
      return BATTERY_CONFIG_STRUCT_SIZE;
    case WIFI_CONFIG_STRUCT:
      return WIFI_CONFIG_STRUCT_SIZE;
  }
}

const getStructName = (struct_id) => {
  switch(struct_id) {
    case HANDSHAKE:
      return 'handshake';
    case STATUS_STRUCT:
      return 'status';
    case BATTERY_STATUS_STRUCT:
      return 'batteryStatus';
    case DDS_CONFIG_STRUCT:
      return 'ddsConfig';
    case ATTENUATORS_CONFIG_STRUCT:
      return 'attenuatorsConfig';
    case BATTERY_CONFIG_STRUCT:
      return 'batteryConfig';
    case WIFI_CONFIG_STRUCT:
      return 'wifiConfig';
  }
}

protocol.define('beep', {
  write: function (data = {
    function: FUNCTION_BEEP,
  }) {
      this.UInt8(BEEP_HEADER)
          .UInt16BE(4)
          .UInt8(data.function)
  },

  read: function() {
    this.UInt8('header')
        .UInt16BE('frame_size')
        .UInt8('function');
  }
});

protocol.define('measureData', {
  write: function({
    timestamp,
    delta,
    count,
    type,
    values
  }) {
    this.UInt8(MEASURE_DATA_HEADER)
        .UInt16BE(13)
        .UInt32BE(timestamp)
        .UInt16BE(delta)
        .UInt16BE(count)
        .UInt8(type)
        .loop(values, this.Values);
  },
  read: function() {
    this.UInt8('header')
        .UInt16BE('frame_size')
        .UInt32BE('timestamp')
        .UInt16BE('delta')
        .UInt16BE('count')
        .Int8('type')
        .loop('values', this.Values, this.context.count);
  }
});

protocol.define('Values', {
  write: function(values) {
    this.UInt16BE(values[0])
        .UInt16BE(values[1])
        .UInt16BE(values[2]);
  },

  read: function() {
    this.loop('values', this.UInt16BE, 3);
    return this.context.values;
  }
});

protocol.define('readData', {
  write: function ({
    struct_id,
  }) {
    this.UInt8(READ_DATA_HEADER)
        .UInt16BE(4)
        .UInt8(struct_id);
  },
  read: function () {
    this.UInt8('header')
        .UInt16BE('frame_size')
        .UInt8('struct_id');
  }
});

protocol.define('writeData', {
  write: function ({
    struct_id,
    struct,
  }) {
    const structSize = getStructSize(struct_id);
    this.UInt8(WRITE_DATA_HEADER)
        .UInt16BE(4 + structSize)
        .UInt8(struct_id)
        .UInt32BE(structSize);
    this[getStructName(struct_id)](struct);
  },
  read: function () {
    this.UInt8('header')
        .UInt16BE('frame_size')
        .UInt8('struct_id')
        .UInt32BE('struct_size');
    this[getStructName(this.context.struct_id)]('struct');
  }
});

protocol.define('beepResponse', {
  write: function (status) {
    this.UInt8(BEEP_RESPONSE_HEADER);
    this.UInt8(status);
  },
  read: function () {
    this.UInt8('header');
    this.UInt8('status');
  }
});

protocol.define('readDataResponse', {
  write: function ({
    struct_id,
    struct,
    status
  }) {
    this.UInt8(READ_DATA_RESPONSE_HEADER)
        .UInt16BE(6 + getStructSize(struct_id))
        .UInt8(struct_id)
        .UInt8(status);
    this[getStructName(struct_id)](struct);
  },
  read: function () {
    this.UInt8('header')
        .UInt16BE('frame_size')
        .UInt8('struct_id')
        .UInt8('status')
    this[getStructName(this.context.struct_id)]('struct');
  }
});

protocol.define('writeDataResponse', {
  write: function ({
    status
  }) {
    this.UInt8(WRITE_DATA_RESPONSE_HEADER)
        .UInt16BE(5)
        .UInt8(status);
  },
  read: function () {
    this.UInt8('header')
        .UInt16BE('frame_size')
        .UInt8('status')
  }
});
