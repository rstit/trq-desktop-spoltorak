const { expect } = require('chai');
const testUtils = require('./utils');
const ScanSetup = require('./ScanSetup');

describe('Scan setup', () => {
  beforeEach(testUtils.beforeEach(ScanSetup));
  afterEach(testUtils.afterEach());

  it('constists of five steps form', async function () {
    this.timeout(60000);
    // First we need to wait for the device to connect
    await this.page.waitUntilConnectedToDevice();
    await this.page.gotoScanSetup();

    // -------------------- MATERIAL SELECT ------------------------- //
    // Are we on the right step?
    expect(await this.page.isCurrentStepOfTotal(1, 5)).to.eq(true);

    // We should not have a possibility to go to the next step without filling the input out.
    await this.page.submitScanStep();
    expect(await this.page.isCurrentStepOfTotal(1, 5)).to.eq(true);

    // No material should be selected
    expect(await this.page.isMaterialSelected()).to.eq(false);

    // Lets fill out the input and submit
    await this.page.selectFirstMaterialOnList();
    await this.page.submitScanStep();

    // -------------------- DETECTION DEPTH ------------------------- //
    // Are we on the right step?
    expect(await this.page.isCurrentStepOfTotal(2, 5)).to.eq(true);

    // We should not have a possibility to go to the next step without filling the input out.
    await this.page.submitScanStep();
    expect(await this.page.isCurrentStepOfTotal(2, 5)).to.eq(true);

    // Input should be empty
    expect(await this.page.isDetectionDepthFilled()).to.eq(false);

    // Lets fill out the input and submit
    await this.page.fillDetectionDepth(1000);
    await this.page.submitScanStep();

    // -------------------- CONFIRM FREQUENCY ------------------------- //
    // Are we on the right step?
    expect(await this.page.isCurrentStepOfTotal(3, 5)).to.eq(true);

    // Input should NOT be empty
    expect(await this.page.isFrequencyFilled()).to.eq(true);

    // Lets fill out the input and submit
    await this.page.confirmFrequency();
    await this.page.submitScanStep();

    // -------------------- PART NUMBER ------------------------- //
    // Are we on the right step?
    expect(await this.page.isCurrentStepOfTotal(4, 5)).to.eq(true);

    // We should not have a possibility to go to the next step without filling the input out.
    await this.page.submitScanStep();
    expect(await this.page.isCurrentStepOfTotal(4, 5)).to.eq(true);

    // Input should be empty
    expect(await this.page.isPartNumberFilled()).to.eq(false);

    // Lets fill out the input and submit
    await this.page.fillPartNumber('TRQ-1000');
    await this.page.submitScanStep();

    // -------------------- DESCRIPTION ------------------------- //
    // Are we on the right step?
    expect(await this.page.isCurrentStepOfTotal(5, 5)).to.eq(true);

    // We should not have a possibility to go to the next step without filling the input out.
    await this.page.submitScanStep();
    expect(await this.page.isCurrentStepOfTotal(5, 5)).to.eq(true);

    // Input should be empty
    expect(await this.page.isDescriptionFilled()).to.eq(false);

    // Lets fill out the input and submit
    await this.page.fillDescription('Scan description');
    await this.page.submitScanStep();

    expect(await this.page.isScanSetup()).to.eq(false);

    // -------------------- GOING BACK ------------------------- //
    // Now lets try to go back to the previous screens:
    await this.page.goBack();
    expect(await this.page.isScanSetup()).to.eq(true);
    expect(await this.page.isCurrentStepOfTotal(5, 5)).to.eq(true);
    expect(await this.page.isDescriptionFilled()).to.eq(true);
    await this.page.goBack();
    expect(await this.page.isCurrentStepOfTotal(4, 5)).to.eq(true);
    expect(await this.page.isPartNumberFilled()).to.eq(true);
    await this.page.goBack();
    expect(await this.page.isCurrentStepOfTotal(3, 5)).to.eq(true);
    expect(await this.page.isFrequencyFilled()).to.eq(true);
    await this.page.goBack();
    expect(await this.page.isCurrentStepOfTotal(2, 5)).to.eq(true);
    expect(await this.page.isDetectionDepthFilled()).to.eq(true);
    await this.page.goBack();
    expect(await this.page.isCurrentStepOfTotal(1, 5)).to.eq(true);
    expect(await this.page.isMaterialSelected()).to.eq(true);
    await this.page.goBack();
    expect(await this.page.isConnection());
    expect(await this.page.isBackButtonVisible()).to.eq(false);
  });

  it('frequency can be edited and is send to the device', async function () {
    this.timeout(60000);
    // First we need to wait for the device to connect
    await this.page.waitUntilConnectedToDevice();
    await this.page.gotoScanSetup();

    // -------------------- SKIP FIRST SCREENS ------------------------- //
    await this.page.selectFirstMaterialOnList();
    await this.page.submitScanStep();
    await this.page.fillDetectionDepth(1000);
    await this.page.submitScanStep();

    // -------------------- EDIT FREQUENCY ------------------------- //
    // Lets fill out the input and submit
    const expectedFrequency = 2;
    await this.page.clickEditFrequencyButton();
    await this.page.fillFrequency(expectedFrequency);
    await this.page.submitScanStep();

    const frequencyFromDevice = await this.device.getCurrentFrequency();
    expect(frequencyFromDevice).to.eq(expectedFrequency);

    // Lets check if we can change the unit of freqency to kHz:
    await this.page.goBack();
    await this.page.clickEditFrequencyButton();
    await this.page.changeFrquencyUnit('kHz');
    await this.page.fillFrequency(expectedFrequency);
    await this.page.submitScanStep();

    const khzFrequencyFromDevice = await this.device.getCurrentFrequency();
    expect(khzFrequencyFromDevice).to.eq(expectedFrequency * 1000);

    // Lets check if we can change the unit of freqency to MHz:
    await this.page.goBack();
    await this.page.clickEditFrequencyButton();
    await this.page.changeFrquencyUnit('MHz');
    await this.page.fillFrequency(expectedFrequency);
    await this.page.submitScanStep();

    const mhzFrequencyFromDevice = await this.device.getCurrentFrequency();
    expect(mhzFrequencyFromDevice).to.eq(expectedFrequency * 1000000);
  });
});
