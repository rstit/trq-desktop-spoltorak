module.exports = class Connection {
  constructor(app) {
    this.app = app;
  }

  waitUntilConnectedToDevice() {
    return this.app.client.waitUntil(async () => {
      return this.isConnectedToDevice();
    }, 60000, 'Cannot connect to device in time');
  }

  waitUntilDisconnectedFromDevice() {
    return this.app.client.waitUntil(async () => {
      return this.isDisconnectedFromDevice();
    }, 60000, 'Cannot disconnect from device in time');
  }

  async isConnectedToDevice() {
    return await this.app.client.getText('main h1') === 'Scanner Connected!';
  }

  async isDisconnectedFromDevice() {
    return await this.app.client.getText('main h1') === 'Scanner not found';
  }
}
