const { expect } = require('chai');
const testUtils = require('./utils');
const Scanning = require('./Scanning');

describe('Scanning', () => {
  beforeEach(testUtils.beforeEach(Scanning));
  afterEach(testUtils.afterEach());

  it('scanning process works', async function () {
    this.timeout(60000);
    // First we need to wait for the device to connect
    await this.page.waitUntilConnected();
    await this.page.gotoScanSetup();
    await this.page.setupScan({
      partNumber: 'TRQ-1000',
      description: 'Scan description',
      detectionDepth: 1000,
      frequency: 123,
    });

    // We should be in idle mode: no measurements frames comming in.
    // Footer should have all proper informations about
    // the scan that will be performed.
    expect(await this.page.isWaitingForMeasurementsFramesFromDevice()).to.eq(true);
    expect(await this.page.isScansListEmpty()).to.deep.eq(true);
    expect(await this.page.getDataFromFooter()).to.deep.eq({
      partNumber: 'TRQ-1000',
      description: 'Scan description',
      manufacturer: '-',
      permeability: '4.12 H/m',
      detectionDepth: '1 mm',
      resistance: '5.33 Ω/cm',
      frequency: '123 Hz',
    });

    // Lets start the scanning and wait 5s, so we can collect some measurements
    await this.device.pressTrigger();
    this.device.startMovingTheDeviceAroud();
    await this.page.wait5s();

    // We shuld be in scanning process:
    expect(await this.page.isWaitingForMeasurementsFramesFromDevice()).to.eq(false);
    // There still is no element on the scan list.
    expect(await this.page.isScansListEmpty()).to.deep.eq(true);

    // And lets stop the scanning process.
    this.device.stopMovingTheDeviceAroud();
    await this.device.releaseTrigger();
    await this.page.wait1s();

    // We should now go back to beign in idle mode and on the scanning list there should be now two items.
    expect(await this.page.isWaitingForMeasurementsFramesFromDevice()).to.eq(true);
    expect(await this.page.getCurrentScansList()).to.deep.eq(['TRQ-1000']);
    expect(await this.page.getDataFromFooter()).to.deep.eq({
      partNumber: 'TRQ-1000',
      description: 'Scan description',
      manufacturer: '-',
      permeability: '4.12 H/m',
      detectionDepth: '1 mm',
      resistance: '5.33 Ω/cm',
      frequency: '123 Hz',
    });

    // Lets create the second scan:
    await this.device.pressTrigger();
    this.device.startMovingTheDeviceAroud();
    await this.page.wait5s();
    await this.device.releaseTrigger();
    this.device.stopMovingTheDeviceAroud();
    await this.page.wait1s();

    expect(await this.page.getCurrentScansList()).to.deep.eq(['TRQ-1000 (1)', 'TRQ-1000']);

    // Lets see if we can edit current scan:
    await this.page.clickScanEditButton();
    expect(await this.page.isEditScanOpened()).to.eq(true);

    expect(await this.page.isEditScanFormPoppulated({
      partNumber: 'TRQ-1000 (1)',
      description: 'Scan description',
    })).to.eq(true);

    // We are now adding a manufacturer
    const manufacturer = 'Foobar';
    await this.page.clickAddManufacturerButton();
    await this.page.addManufacturer(manufacturer);

    expect(await this.page.getManufacturer()).to.eq(manufacturer);

    // And we are able to add some additional info:
    const key0 = 'Foo0';
    const key1 = 'Foo1';
    const value0 = 'Bar0';
    const value1 = 'Bar1';
    await this.page.clickAddAdditionalInfoButton();
    await this.page.setAdditionalInfo(0, key0, value0);
    await this.page.clickAddAdditionalInfoButton();
    await this.page.setAdditionalInfo(1, key1, value1);
    expect(await this.page.getAdditionalInfo(0)).to.deep.eq({
      key: key0,
      value: value0,
    });
    expect(await this.page.getAdditionalInfo(1)).to.deep.eq({
      key: key1,
      value: value1,
    });

    // Can we delete additional info?
    await this.page.deleteAdditionalInfo(1);
    expect(await this.page.isAdditionalInfoVisible(1)).to.eq(false);

    // Can we cancel?
    await this.page.cancelEditScan();
    expect(await this.page.isEditScanOpened()).to.eq(false);

    // We can go to scan summary
    await this.page.finishScanning();
    const isScanSummaryOpened = await this.page.isScanSummaryOpened();
    expect(isScanSummaryOpened).to.eq(true);

    // And it should present all the scans we've made:
    expect(await this.page.getScansSummaryList()).to.deep.eq(['TRQ-1000 (1)', 'TRQ-1000']);


    const yes = true;
    await this.page.mockConfirmationDialogWithAnswer(yes);

    // We can remove one of those scans:
    await this.page.deleteScanNthScanOnSummaryList(1);

    expect(await this.page.getScansSummaryList()).to.deep.eq(['TRQ-1000']);

    // Can we go back to scanning process?
    await this.page.goBack();

    expect(await this.page.isScanningProcess()).to.eq(true);

    // We have only one scan after deletion of the other one?
    expect(await this.page.getCurrentScansList()).to.deep.eq(['TRQ-1000']);

    // Let's go to scan summary again and save this scan
    await this.page.finishScanning();
    await this.page.saveScans();

    // We should be in recent scans:
    expect(await this.page.isRecentScans()).to.eq(true);

    // We should have rect scan marked as green
    expect(await this.page.getRecentScans()).to.deep.eq(['TRQ-1000']);

    // And when I click "Start new scan" I'm redirected to the proper view
    await this.page.clickStartNewScan();
    expect(await this.page.isScanSetup()).to.eq(true);
  });
});
