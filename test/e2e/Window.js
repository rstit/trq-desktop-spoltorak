const Connection = require('./Connection');
module.exports = class Window extends Connection {
  constructor(app) {
    super(app);
  }

  static createSleepPromise(ms) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    })
  }

  waitUntilConnected() {
    return this.app.client.waitUntil(async () => {
      const text = await this.app.client.getText('main h1');
      return text === 'Scanner Connected!';
    }, 60000, 'Cannot connect to device in time');
  }

  getTitle() {
    return this.app.client.getText('.titlebar-title');
  }

  isDevToolsOpened() {
    return this.app.client.browserWindow.isDevToolsOpened();
  }

  minimize() {
    return this.app.client.click('.titlebar-minimize');
  }

  maximize() {
    return this.app.client.click('.titlebar-resize');
  }

  isMaximized() {
    return this.app.client.browserWindow.isMaximized();
  }

  isMinimized() {
    return this.app.client.browserWindow.isMinimized();
  }

  wait5s() {
    return Window.createSleepPromise(5000);
  }

  wait2s() {
    return Window.createSleepPromise(2000);
  }

  wait3s() {
    return Window.createSleepPromise(3000);
  }

  wait1s() {
    return Window.createSleepPromise(1000);
  }

  getErrorMessage() {
    return this.app.client.getText('trq-alert .alert.error');
  }

  async mockConfirmationDialogWithAnswer(answer = false) {
    return await this.app.webContents.executeJavaScript(`window.confirm = function (message) { return ${answer} }`);
  }
}
