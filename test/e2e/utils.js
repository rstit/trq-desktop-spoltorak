const electron = require("electron");
const { Application } = require("spectron");
const device = require("../../fake-device/device");

const {
  DEVICE_PORT,
  FAKE_DEVICE_HOST,
} = require('../../src/backend/constants');

const openDevice = (device) => {
  return new Promise(resolve => {
    device.listen(DEVICE_PORT, FAKE_DEVICE_HOST, () => {
      resolve();
    });
  });
}

const closeDevice = (device) => {
  return new Promise((resolve) => {
    device.unref();
    if (!device.destroyed) {
      device.close(() => {
        resolve();
      });
    }
    device.forceClientDisconnect();
    if (device.destroyed) {
      resolve();
    }
  });
}

const beforeEach = function(PageObject) {
  return async function() {
    this.timeout(10000);
    this.device = device;
    await openDevice(this.device);
    this.app = new Application({
      path: electron,
      args: ["."],
      startTimeout: 10000,
      waitTimeout: 10000
    });
    await this.app.start();
    this.page = new PageObject(this.app);
  }
};

const afterEach = function() {
  return async function() {
    this.timeout(10000);
    await closeDevice(this.device);
    if (this.app && this.app.isRunning()) {
      return this.app.stop();
    }
    return undefined;
  }
};

module.exports = {
  beforeEach,
  afterEach,
};

